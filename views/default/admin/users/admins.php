<?php
/**
 * Admins admin page
 */

$limit = 10;
$offset = max((int)get_input('offset', 0), 0);

$admins = elgg_get_admins(array(
	'limit' => $limit,
	'offset' => $offset,
));

$count = elgg_get_admins(array('count' => 'true'));
$nav = elgg_view('navigation/pagination', array(
	'limit' => $limit,
	'offset' => $offset,
	'count' => $count,
));

$content = '<ul class="elgg-list">';
foreach ($admins as $admin) {
	$icon = elgg_view_entity_icon($admin, 'tiny', array('use_hover' => 'true'));

	$admin_title = elgg_view('output/url', array(
		'href' => $admin->getURL(),
		'text' => $admin->name,
		'is_trusted' => true,
	));

	$remove_admin_url = "action/admin/user/removeadmin?guid={$admin->guid}";
	$remove_admin_button = elgg_view('output/url', array(
		'href' => $remove_admin_url,
		'text' => elgg_echo('removeadmin'),
		'class' => 'elgg-button elgg-button-submit',
		'is_action' => true,
		'is_trusted' => true,
	));

	$body = "<h3>$admin_title</h3>";
	if($admin->guid != elgg_get_logged_in_user_guid()){
		$alt = $remove_admin_button;
	} else {
		$alt = "";
	}

	$content .= '<li class="pvs">';
	$content .= elgg_view_image_block($icon, $body, array('image_alt' => $alt));
	$content .= '</li>';
}
$content .= '</ul>';

echo elgg_view_module('inline', elgg_echo('admins'), $content . $nav);
