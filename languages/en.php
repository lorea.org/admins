<?php
/**
 * Admins English language file.
 *
 */

$english = array(
	'admins' => 'Admins',
	'admin:users:admins' => 'Admins',
);

add_translation('en', $english);
