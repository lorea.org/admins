<?php
/**
 * Admins
 *
 * @package ElggAdmins
 *
 * 
 */

elgg_register_event_handler('init', 'system', 'admins_init');

/**
 * Init admins plugin.
 */
function admins_init() {

	// add an admin navigation menu item
	elgg_register_admin_menu_item('administer', 'admins', 'users');
	
}
